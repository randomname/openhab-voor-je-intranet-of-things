OpenHAB voor je Intranet of Things
==================================

Inleiding
---------

![](images/openHABLogo.png)

(Figuur 1: OpenHAB logo)

Als je nog nooit gehoord hebt van OpenHAB dan adviseer ik je eerst het artikel
'*Het smarthome komt naar je thuis*' van Jasper Bakker' te lezen
([tinyurl.com/openhab-smart](http://tinyurl.com/openhab-smart)). Ik ga in dit artikel dieper
in op de concepten en techniek achter OpenHAB en probeer je met een paar
voorbeelden te inspireren om je huis (en je leven) verder te verrijken met
behulp van dit geweldige opensource project dat al een aantal awards op zijn
naam heeft staan.

OpenHAB: de feiten
------------------

-   OpenHAB staat voor Open Home Automation Bus en daarin draait het om de
    integratie van allerlei soorten devices.

-   Het project is in 2010 gestart door de Duitser Kai Kreuzer. Zijn filosofie:
    *Huisautomatisering is om te zorgen voor comfort, veiligheid en
    energiebesparing: maak je huis slim, gebruik makend van de diverse apparaten
    die je al in huis hebt.*

-   De OpenHAB server draait op Mac, Windows en Linux OS en kan qua hardware
    specificaties al af met een Raspberry Pi.

-   OpenHAB moet je configureren, en daarvoor is de OpenHAB Designer beschikbaar
    (Mac, Windows en Linux desktop).

-   OpenHAB heeft een heel actieve community van developers, supporters en
    gebruikers.

It's all about the Items and Rules
----------------------------------

Om OpenHAB effectief te kunnen inzetten voor het automatiseren van je devices is
het belangrijk om de concepten van OpenHAB te begrijpen en hoe ze om te zetten
zijn in de configuratiebestanden die OpenHAB nodig heeft om je huis slimmer te
maken. In figuur 2 worden deze concepten aan elkaar gerelateerd.

![](images/itemsRulesDevices2.png)

(Figuur 2: Samenhang tussen Items, Rules, Devices en Sitemaps)

De inhoud van de gekleurde blokjes in het plaatje wordt zo dadelijk verder
toegelicht, maar in het kort komt het neer op het volgende: Items staan centraal
in OpenHAB en kunnen gekoppeld worden aan Devices middels Bindings. Met Rules
kan vervolgens gereageerd worden op veranderingen die optreden in de Items. Voor
de bediening van Items en het weergeven van de toestand van Items kunnen
gebruikersinterfaces gedefinieerd worden met Sitemaps. Deze Sitemaps kun je dan
bijvoorbeeld gebruiken op een smartphone (Android of Apple), een browser of
zelfs een Pebble horloge.

Items zijn de kleinste 'Things' die betekenis voor jou hebben en waarvan je de
toestand wilt bijhouden. Hierbij kun je denken aan de temperatuur van een sensor
of de kleur van een ledlamp. Binnen OpenHAB zijn er verschillende type Items,
waarvan de naam vaak al aangeeft waar ze voor bedoeld zijn: Switch, Contact,
Number, String, DateTime, Dimmer en Color. Een item kent 'Status updates' en
zelf kun je 'Commands' sturen naar een Item. In figuur 3 zie je dat
geïllustreerd voor een Item van het type Dimmer. Deze 'Commands' (ON / OFF / x %
/ INC(rement) en DEC(rease)) en 'Status updates' (ON / OFF / x %) kunnen
verschillen per Item Type.

![](images/ItemExplained.png)

(Figuur 3: Item)

Het hart van OpenHAB bestaat uit een event bus. Hierin komen alle wijzigingen en
updates op de State van Items binnen en worden ze opgenomen in het Item
Registry, een direct opvraagbare lijst van items en statussen.

Afhankelijk van het type Item kun je er bepaalde 'Commands' op uitvoeren en is
het logisch om er bepaalde 'Status updates' op terug te krijgen.

Rules daarentegen zijn 'de lijm van OpenHAB' waarmee je alles aan elkaar kunt
knopen en een hogere intelligentie-laag over je Items kunt leggen. Rules werken
doordat ze kunnen reageren op zogenaamde triggers. Deze triggers zijn:

-   Een *Item(-Event)-based trigger*: als het Item een 'Command' of 'Status
    update' ontvangt.

-   Een *System-based trigger*: bij het opstarten of afsluiten van OpenHAB.

-   Een *Time-based triggers*: op bepaalde tijdstippen volgens een
    cron-expressie die ook seconden ondersteunt.

Configuratie
------------

Nu duiken we verder in op de configuratie. OpenHAB kent een 'configurations'
folder waarin onder meer Items, Rules en Sitemaps geconfigureerd kunnen worden.

### Items

Items wordt in de configuratie van OpenHAB bekend gemaakt door een textfile
eindigend op .items. Een Item-definitie plaats je doorgaans op één regel. In
figuur 2 zie je dat een tweetal Items gedefinieerd zijn. Allereerst een 'Lamp
entree' van het ItemType Switch die via een KNX binding gekoppeld is aan een
device. Daarnaast is er de Deurbel van het ItemType 'Contact' welke via een
ZWave binding gekoppeld is aan een device.

De syntax van een Item is als volgt:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
itemtype itemname ["labeltext"] [<iconname>] [(group1, group2, ...)] [{bindingconfig}  
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Een item is minimaal van een bepaald eerder genoemd ItemType en kent een
technische naam (“itemname"). Daarnaast wordt het item optioneel nog
gerepresenteerd door een bepaalde tekst (“labeltekst”), wordt in een GUI
weergegeven met een icoontje (\<iconname\>), wordt geplaatst in één of meerdere
groepen (group1, group2, …) en wordt last but not least (optioneel) gekoppeld
aan een binding ({bindingconfig}).

### Rules

Rules wordt in de configuratie van OpenHAB bekend gemaakt door een textfile
eindigend op .rules. De Rule *"Deurbel? LampEntree aan"* in figuur 2 zorgt
ervoor dat de lamp aan gaat ("sendCommand") op het moment dat iemand op de
deurbel drukt (“when Deurbel received update OPEN”).

De syntax van Rules zijn gebaseerd op Xbase. Xbase is een 'statically typed
expression language for Java' die geschreven is in Xtext. Wat dat betekent is
dat er een domain specific language speciaal voor OpenHAB rules is en waarbij de
OpenHAB Designer (welke gebaseerd is op Eclipse) je voorziet van een parser,
linker, typechecker en compiler.

Een rule kan als deze getriggerd wordt een aantal acties uitvoeren. Veel
voorkomende actions die al standaard beschikbaar zijn in OpenHAB zijn
bijvoorbeeld *sendCommand* voor het sturen van een 'Command' naar een Item,
*postUpdate* voor het sturen van een 'Status update' naar een Item en *sendMail*
voor het versturen van een email.

### Userinterfaces met Sitemaps

Sitemaps zijn een ander stukje van de OpenHAB configuratie puzzel. Met deze
sitemap kan dan on-the-fly een grafische userinterface (GUI) gemaakt worden,
welke op meerdere type devices te bekijken is. Een slim ontworpen userinterface
(UI) is belangrijk voor het gebruikersgemak en ook een belangrijke factor voor
adoptie van de huisautomatisering door medebewoners, met name als je te ook
maken hebt met de Wife Acceptance Factor (WAF) ;-) Je hebt alle vrijheid om de
UI toe te spitsen op de gebruiker en/of de ruimte(s) die aangestuurd moeten
worden. Zo hou je de bediening overzichtelijk en efficiënt.

In je sitemap geef je aan welke Items getekend moeten worden en afhankelijk van
het type en het soort device (web, mobile) wordt dan een op het ItemType
toegepast bedieningselement getoond. Bijvoorbeeld een Switch, die wordt in de UI
vertaald naar een aan/uit knop. Een Color wordt daarentegen getoond middels een
kleurenkiezer (color wheel) en voor een Dimmer wordt een slider getekend.

Van deze syntax zal ik geen voorbeeld laten zien aangezien deze uitgebreid
beschreven is in de documentatie van OpenHAB. Om een indruk te krijgen van de
mogelijkheden: op Android ziet een user interface er met HABDroid er
bijvoorbeeld uit zoals in figuur 4.

![](images/habdroid_sample.png)

(Figuur 4: OpenHAB interface op Android met HABDroid)

Als je nog een bijna afgeschreven Android of iOS smartphone in je bezit hebt kun
je deze voorzien van respectievelijk HABDroid of OpenHAB en deze aan de muur
ophangen in bijvoorbeeld je woonkamer. Totdat we goed werkende stembediening als
alternatief hebben kan dit een mooie manier zijn om je smarthome te bedienen.

Bindings
--------

Bindings zijn dè manier om je Items te koppelen aan de diverse devices, omdat ze
middels status updates het Item bijwerken, maar ook commands die uitgevoerd
worden op het Item kunnen uitvoeren op het gekoppelde device.

>   Wat OpenHAB zo krachtig maakt is dat er op dit moment al voorbij de honderd
>   bindings zijn voor veelvoorkomende devices en (online) diensten. Dit zorgt
>   ervoor dat een groot aantal devices snel en eenvoudig uit te lezen en/of te
>   besturen zijn.

Er zijn een aantal type bindings. De meeste zijn bedoeld om de koppeling met een
device te leggen, maar een aantal zijn bedoeld om acties uit te kunnen voeren
(Action bindings) of om veranderingen in Items op te slaan (Persistence
bindings). Bij Action bindings kun je denken aan bijvoorbeeld het versturen van
een chatbericht of een notification. Bij persistence moet je denken aan opslag
van Item-statussen in MySQL, InfluxDB, rrd4j of direct op de disk met MapDB.

Wil je zelf een binding maken? OpenHAB is er goed op ingericht om uitgebreid te
worden middels bindings, maar dit is bijna niet nodig. Voor de meest voorkomende
bindings en acties zijn namelijk al modules beschikbaar. Een voorbeeld van een
binding is bijvoorbeeld MyOpenHAB waarmee je kunt koppelen met de welbekende IF
This Then That (IFTTT) site, of de Hue binding waarmee je je Philips Hue lampen
kunt uitlezen en besturen.

Scripts, Persistence en Transforms
----------------------------------

Naast Items, Rules, Sitemaps en Bindings zijn er ook nog Scripts, Persistence en
Transforms. Willekeurige scripts kun je simpelweg toevoegen in de gelijknamige
configuratie folder en deze eenvoudig aanroepen vanuit een Rule. Voorwaarde is
dan wel dat deze scripts executeerbaar moeten zijn op het operating system
waarop OpenHAB gedraaid wordt.

Per vorm van gegevensopslag (Persistence) die je gebruikt kan middels een
settings file ingesteld worden welke changes of updates op Items opgeslagen
moeten worden. Ook bestaat de mogelijkheid om met een crontab expressie
periodiek de status van Items op te slaan. Wat persistence betreft heb je
behoorlijk wat keus, met bijvoorbeeld db4o, InfluxDB, alles dat werkt met JDBC
(o.a. MySQL), MapDB (lokaal op disk), MongoDB, rrd4j (handig voor grafieken) en
zelfs een MQTT bus.

Gegevens wil je idealiter opslaan zodat je er grafieken van kunt genereren,
berekeningen op los kunt laten, of om de laatste waarde van elk Item weer in te
laden na een herstart van OpenHAB. Ook als je nog niet precies weet wat je met
de historische gegevens wilt doen is het meestal een goed idee om deze op te
slaan voor het geval je hier later nog wat mee wilt doen.

Transforms zijn niets anders dan simpele mapping-tabellen of xml bestanden met
XSLT translaties die een waarde vertalen naar een andere waarde, meestal tekst
die bedoeld is om te gebruiken in een gebruikers-interface.

Zelf aan de slag
----------------

Als je zelf meer bekend wilt raken met OpenHAB, dan is het handig om eerst
bekend te raken met de bestaande 'configuration'-folder. Je doet er goed aan om
te beginnen met de demo configuration zodat je begint met een werkende situatie.
Dat kan gelukkig snel en eenvoudig met behulp van Docker:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker run -d -p 8080:8080 --name=openhab2 wetware/openhab2
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Vervolgens kun je via http://host-ip:8080 direct spelenderwijs kennis maken met
OpenHAB.

### OpenHAB inrichten

Om aansluitend verder aan de slag te gaan met OpenHAB kun je het beste de
'getting started' guideline volgen (<http://www.openhab.org/getting-started>).

De eerdergenoemde OpenHAB Designer (figuur 5) is erg handig om alle settings te
bewerken. Daarvoor moet je dan wel op het filesysteem bij de configuratie folder
kunnen komen, bijvoorbeeld met 'file and print sharing' als je de OpenHAB
Designer wilt gebruiken vanaf een andere PC.

![](images/designer2.png)

(Figuur 5: OpenHAB Designer)

OpenHAB versie 2
----------------

OpenHAB 2 is de opvolger van de huidige stabiele versie en die is momenteel nog
volop in ontwikkeling. In deze nieuwste release ligt de focus op het verbeteren
van het gebruiksgemak, zodat de bruikbaarheid toeneemt voor minder technische
mensen. Deze versie bevat bijvoorbeeld discovery features waarmee OpenHAB een
aantal devices op je intranet zelf kan vinden en kan configureren. Als je wilt
mee helpen ontwikkelen aan OpenHAB dan zul je merken dat OpenHAB 1.x en 2.x al
sterk verweven zijn. Veel modules uit OpenHAB 1.x zijn compatible met OpenHAB
2.x (OH2), en bij het starten van het project start je standaard de OH2 binary
in een backwards compatibility modus.

Awesome Intranet of Things
--------------------------

Met het behandelen van alle belangrijke concepten is de basis gelegd voor het
kunnen realiseren van vele awesome toepassingen. Maar wat zou je allemaal kunnen
automatiseren? Op OpenHAB community website en op vele andere plaatsen op het
Internet zijn een geweldige bron van inspiratie. Daarnaast kun je voor een
uitgebreidere versie van dit artikel terecht op mijn blog:
[tinyurl.com/openhab-more](http://tinyurl.com/openhab-more)

Waar wacht je nog op? Een awesome Intranet of Things kan werkelijkheid worden.
OpenHAB biedt een solide, betrouwbare en krachtige basis waarmee je veel devices
zonder moeite kunt aansluiten en je huis nog slimmer kunt maken. Het kost
misschien wat tijd om OpenHAB precies te laten doen wat jij wilt en bekend te
raken met de configuratie, maar je zult zien dat de tijdsinvestering zich
terugverdient als je de kracht van OpenHAB ontdekt en je awesome ideeën
werkelijkheid worden.

Referenties
-----------

| Het smarthome komt naar je thuis   | <http://tinyurl.com/openhab-smart>       |
|------------------------------------|------------------------------------------|
| OpenHAB source code                | <https://github.com/openhab>             |
| OpenHAB community forum            | <https://community.openhab.org>          |
| Officiële OpenHAB site             | <http://www.openhab.org>                 |
| Live demo                          | <http://demo.openhab.org>                |
| Getting started                    | <http://www.openhab.org/getting-started> |
| OpenHAB voor je Intranet of Things | <http://tinyurl.com/openhab-more>        |
